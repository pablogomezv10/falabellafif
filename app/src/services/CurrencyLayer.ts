import {defaults} from 'request-promise-native';

const BASE_URL_HTTP = 'http://api.currencylayer.com'

export class CurrencyLayerClient {

    private baseUrl: string;
    private _apiKey: string;
    private client: any;

    constructor(apiKey: string){
        this.baseUrl = BASE_URL_HTTP
        this._apiKey = apiKey;
        this.client = defaults({
            baseUrl: this.baseUrl,
            qs: {
                access_key: this._apiKey
            },
            json: true
        })
    }

    public convert = async(from: string, to: string, amount: number)=>{
        try{
            const converted = await this.client.get({uri: '/convert', qs: {from, to, amount}});
            return converted;
        }catch(e){
            console.log(e);
            return amount;
        }
    }

}