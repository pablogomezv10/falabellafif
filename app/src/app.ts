import "reflect-metadata";
import express, { application, Express, Application } from 'express'
import BeerRouter  from "./route/beerRoutes";
import { createConnection } from "typeorm";

const app: Application = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/beer', BeerRouter)

createConnection().then(()=>{
    app.listen(3000, ()=>{
        console.log('Running');
    });
})

