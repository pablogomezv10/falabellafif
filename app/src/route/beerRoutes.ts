import { Router } from 'express';
import { BeerController } from '../controller/beerController';

class BeerRouter {

    public router: Router = Router({caseSensitive: true});
    private controller: BeerController = new BeerController();

    constructor(){
        this.getBeer();
        this.getBeers();
        this.addBeer();
        this.getBoxBeer();
    }

    getBeers(){
        this.router.get('/', this.controller.getAll);
    }

    getBeer(){
        this.router.get('/:id', this.controller.get);
    }

    getBoxBeer(){
        this.router.get('/:id/boxprice', this.controller.getBox);
    }

    addBeer(){
        this.router.post('/', this.controller.add);
    }
}

const beerRouter = new BeerRouter();
export default beerRouter.router;