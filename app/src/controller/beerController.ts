import { BeerItem, Beer } from '../model/beer'
import { Request, Response } from 'express';
import { BeerBox } from '../model/beerbox';
import { CurrencyLayerClient } from '../services/CurrencyLayer';

export class BeerController {
    public currencyLayer: CurrencyLayerClient;

    constructor(){
        this.currencyLayer = new CurrencyLayerClient('bfe77c94f6069ed97b58c128d4cb46f6')
    }

    public getBox = async(req: Request, res: Response) => {
        let { id } = req.params;
        const { currency  } = req.query;
        let { quantity } = req.query;
        if(quantity == undefined) quantity = 6;

        if (await this.exist(parseInt(id), res)) res.status(404).end();
        const beer = await BeerItem.findOne({where:{id:id}});
        let total: any = beer?.price*quantity;
        if(currency){ 
            total = await this.currencyLayer.convert(beer?.currency, currency, total)
        }
        let beerBox: BeerBox = {totalPrice: `${!currency? total: total.result} ${!currency ? beer?.currency : currency}`}
        res.status(200).json(beerBox).end();
    }

    public get = async (req: Request, res: Response) => {
        try{
            const { id } = req.params;
            if ((await this.exist(parseInt(id), res))) res.status(404).end();
            const beer = await BeerItem.findOne({where:{id: id}});
            res.status(200).json(beer).end()
        }catch(e){
            console.log(e);
            res.status(500).end();
        }
    }

    public getAll = async (req: Request, res:Response) => {
        try{
            const beers = await BeerItem.find();
            res.status(200).json(beers);
        }catch(e){
            console.log(e);
            res.status(500).end();
        }
    }

    public add = async (req: Request, res: Response) => {
        try{
            await BeerController.postMethodNotInHeaders(req, res);
            let beer: Beer = req.body;
            console.log(await this.exist(beer.id, res));
            if(!await this.exist(beer.id, res)) res.status(409).json({msg: 'La cerveza existe'}).end();
            await this.createAndSave(beer, res);
        }catch(e){
            console.log(e);
            res.status(500).end();
        }
    }

    private exist = async (id: number | undefined, res: Response)=>{
        if(id == undefined){
            res.status(400).end();
        }
        try{
            const beer = await BeerItem.findOne({where: {id: id}});
            if(beer!==undefined) return false;
            return true
        }catch(e){
            console.log(e);
            res.status(500).end();
        }
        
    }

    private createAndSave = async(item: Beer, res: Response) =>{
        try{
            let itemBeer: BeerItem = BeerItem.create(item);
            itemBeer = await itemBeer.save();
            if(!itemBeer){
                new Error('No se pudo salvar la cerveza')
            }
            res.status(201).json({msg: 'Cerveza añadida'}).end();    
        }catch(e){
            console.log(e);
        }
        res.status(500).end();
    }

    static async postMethodNotInHeaders(req: Request, res: Response){
        if(req.method !== 'POST'){
            res.status(400).json({status:'Error', msg: "Problemas con el metodo llamado"}).end()
        }
    }
}