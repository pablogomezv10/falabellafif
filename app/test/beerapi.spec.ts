import { describe, it} from 'mocha';
import { request, expect, use } from 'chai';
import chaiHttp = require('chai-http');
import { Beer } from '../src/model/beer';
import { doesNotMatch } from 'assert';

use(chaiHttp)

describe('ApiRest', ()=>{

    let url = 'http://localhost:3000'
    let beer: Beer = {
        id: Math.floor(Math.random()*1000)*1,
        name: 'Testing',
        brewery: 'Test',
        country: 'Chile',
        price: 1000,
        currency: 'CLP'
    };
    let quantity = Math.floor(Math.random()*24)+1
    describe('/beer', ()=>{

        afterEach(()=>{
            setTimeout(()=>{}, 10000);
        })

        it("#POST should return status 201", function(done){
            request(url)
                .post("/beer")
                .send(beer)
                .end((err, res)=>{
                    expect(err).to.be.null;
                    expect(res).to.have.status(201);
                    done(err);
                });
        })


        it("#GET should return a status 200", function(done){
            request(url)
                .get('/beer')
                .end((err, res)=>{
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    done(err)
                });
        })


        it("#POST should return a status 409", function(done){
            request(url)
                .post("/beer")
                .send(beer)
                .end((err,res)=>{
                    expect(err).to.be.null;
                    expect(res).to.have.status(409);
                    done(err);
                })
        })
    })

    describe('/beer/:id', ()=>{
        it('#GET should return a status 200', (done)=>{
            request(url)
                .get(`/beer/${beer.id}`)
                .end((err, res)=>{
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    done(err)
                })
        });

        it('#Should return a status 404', (done)=>{
            request(url)
                .get('/beer/10000')
                .end((err, res)=>{
                    expect(err).to.be.null;
                    expect(res).to.have.status(404);
                    done(err)
                })
        })
    })

    describe('/beer/:id/boxprice', ()=>{
        it('#GET should return a status 200', (done)=>{
            request(url)
                .get(`/beer/${beer.id}`)
                .end((err, res)=>{
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    done(err)

            })
        });

        it('#GET should return a status 404', (done)=>{
            request(url)
                .get(`/beer/10000`)
                .end((err, res)=>{
                    expect(err).to.be.null;
                    expect(res).to.have.status(404);
                    done(err);
            })
        })

        it(`#GET should be ${beer.price}*${quantity} = ${beer.price*quantity} correct`, (done)=>{
            request(url)
                .get(`/beer/${beer.id}/boxprice?quantity=${quantity}`)
                .end((err,res)=>{
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("totalPrice", beer.price*quantity);
                    done(err);
                })
        })
    })
})